import java.applet.Applet;
import java.awt.Graphics;


public class HelloWorldApplet extends Applet {
    public void paint(Graphics g) {
        g.drawString("Hello, World!", 50, 50);
    }
}

//myapplet.html
<!DOCTYPE html>
<html>
<head>
    <title>Hello, World! Applet</title>
</head>
<body>
    <applet code="HelloWorldApplet.class" width="200" height="200">
        Your browser does not support Java applets.
    </applet>
</body>
</html>
