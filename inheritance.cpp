#include<iostream>
using namespace std;
class GrandMother   //base class
{
  public:
  GrandMother()
  {
      cout << "GrandMother() is called..." << endl;
  }
};
class Father : public GrandMother //Father class inheriting the properties of GrandMother class
{
    public:
    Father()


{
     cout << "Father() inherits from GrandMother()..." << endl;   
    }
};
class Mother //base class
{
    public:
    Mother()
    {
        cout << "Mother() is called..." << endl;
    }
};
class Son : public Father,public Mother    
{};
class Daughter : public Father
{
    public:
    Daughter()
    {
        cout << "Daughter() inherits from Father()..." << endl;
    }
};
class Brother : public Mother{}; 
class Sister : public Mother{};  
class GrandFather  
{
  public:
  GrandFather()
  {
      cout << "GrandFather() is called..." << endl;
  }
};
class GrandDaughter : public Father, public GrandFather{}; 
class Baby : public GrandDaughter{};  
//main function
int main()
{
    cout << "single inheritance" << endl;
   Father obj;
    cout << "\multiple inheritance" << endl;
    Son obj1;
    count << "\multilevel inheritance" << endl;
    Daughter obj2;
    count << "\Hierarchical inheritance" << endl;
    Brother obj3;
    Sister obj4;
    cout << "\nHybrid inheritance" << endl;
    GrandDaughter obj5;
    Baby obj6;
    return 0;
}

