public class AccessSpecifier {
    
    private int pariVar;      
    protected int proVar;   
    public int pubVar;      
    public void setVar(int priValue, int proValue, int pubValue) {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
   public void getVar() {
        System.out.println("Private Variable: " + priVar);
        System.out.println("Protected Variable: " + proVar);
        System.out.println("Public Variable: " + pubVar);
    }
    
    public static void main(String[] args) {
        AccessSpecifier obj = new AccessSpecifier();
        obj.setVar(10, 20, 30);   
        obj.getVar();             
    }
