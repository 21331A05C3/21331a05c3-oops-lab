class MyException extends Exception{
    public MyException(String s)
    {
        super(s);
    }
}
public class Ex2 {
    public static void main(String[] args) throws MyException{
        try{
            throw new MyException("it is user defined exception");
        }
        catch(MyException ex)
        {
            System.out.println("It is default " + ex);
        }
       
    }
}
