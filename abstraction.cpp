#include<iostream>
using namespace std;
class Smartphone
{
    virtual void TakeASelfie()=0;
};
class Iphone : public Smartphone{
    public:
   void TakeASelfie()
   {
    cout<<"Iphone selfie.."<<endl;
   }
};
class Realme: public Smartphone{
    public:
     void TakeASelfie()
     {
        cout<<"Realme Selfie.."<<endl;
     }
};
int main()
{
    Iphone obj;
    obj.TakeASelfie();
    Realme obj1;
    obj1.TakeASelfie();
}
