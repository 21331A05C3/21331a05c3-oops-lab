/*Acess modifier Public*/
#include<iostream>
using namespace std;
class Class1
{
   
    public:
    void value(int n)
    {
        cout<<"value is "<<n<<endl;
    }
};
int main()
{
    Class1 obj;
    obj.value(10);
}



//PRIVATE
/*Acess modifier Private*/
#include<iostream>
using namespace std;
class Class1
{
 
    private:
    string name;
    public:
    void value(string n)
    {
      name=n;
      cout<<"Accessmodifier is "<<name<<endl;
    }
};
int main()
{
    Class1 obj;
    obj.value("Private");
}  
//PROTECTED
/*Acess modifier Protected*/
#include<iostream>
using namespace std;
class Class1
{
   
    protected:
    string name;
   
};
class Class2:public Class1{
    public:
    void value(string n)
    {
      name=n;
      cout<<"Accessmodifier is "<<name<<endl;
    }
};
int main()
{
    Class2 obj;
    obj.value("Protected");
}  
