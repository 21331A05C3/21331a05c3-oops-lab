#include<iostream>
using namespace std;
class Calculator
{
    public:
    int add(int x)
    {
        return ++x;
    }
    int add(int x,int y)
    {
        return x+y;
    }
    int add(int x,int y,int z)
    {
        return x+y+z;
    }
};
int main()
{
    Calculator obj;
    cout<<"increment is "<<obj.add(5)<<endl;
    cout<<"addition of two number is "<<obj.add(4,5)<<endl;
    cout<<"addition of three numbers is "<<obj.add(1,2,3)<<endl;
}


