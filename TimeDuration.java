import java.time.Duration;
import java.time.temporal.ChronoUnit;
public class Duranewtion {
    public static void main(String[] args) {
        Duration duration1 = Duration.ofHours(2);
        int hashCode1 = duration1.hashCode();
        System.out.println("HashCode of duration1: " + hashCode1);
        Duration duration2 = Duration.ofHours(2);
        int hashCode2 = duration2.hashCode();
        System.out.println("HashCode of duration2: " + hashCode2);
        long hours = 3;
        Duration durationFromHours = Duration.from(ChronoUnit.HOURS.getDuration().multipliedBy(hours));
        System.out.println("Duration from " + hours + " hours: " + durationFromHours);
        Duration duration3 = Duration.ofMinutes(30);
        Duration duration4 = Duration.ofMinutes(45);
        int comparisonResult = duration3.compareTo(duration4);
        System.out.println("Comparison result: " + comparisonResult);
        Duration duration5 = Duration.ofMinutes(-30);
        Duration absoluteDuration = duration5.abs();
        System.out.println("Absolute duration: " + absoluteDuration);
    }
}
